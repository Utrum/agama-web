export const proxyServers = [{
  ip: 'electrum1.utrum.io',
  port: 9999,
  ssl: true,
}];

export default proxyServers;
