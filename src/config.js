// web app config
let Config = {
  version: '0.1.5-beta',
  debug: true,
  defaultLang: 'EN',
  roundValues: false,
  // single coin option
  whitelabel: true,
  wlConfig: {
    title: 'Utrum web wallet', // app title
    mainLogo: 'cryptologo/oot.png', // login logo
    coin: {
      ticker: 'OOT',
      name: 'Utrum',
      logo: 'cryptologo/oot.png', // dashboard coin logo
    },
    explorer: 'https://explorer.utrum.io', // insight or iquidus
    serverList: [ // electrum servers list
      'electrum1.utrum.io:10088:tcp',
      'electrum2.utrum.io:10088:tcp',
    ],
  },
};

export default Config;
